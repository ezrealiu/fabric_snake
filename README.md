# fabric_snake

This template should help get you started developing with fabric and  Vue 3 in Vite.

## node
version 18.20.4

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
